$('#search-button').click(function () {
    $.ajax({
        type: "GET",
        url: 'search',
        data: {keywords: $('#search-input').val()},
        success: function (data) {
            let result = '<div class="row">';

            if (data.length === 0) {
                result += '<p>Sorry, no results were found for your search</p>';
            } else {
                data = JSON.parse(data);

                data.forEach(function (video) {
                    let link = `https://www.youtube.com/watch?v=${video.id.videoId}`;
                    result += `
                    <div class="card p-md-2" style="width: 18rem;">
                      <img class="card-img-top" src="${video.snippet.thumbnails.medium.url}" alt="Image sample">
                      <div class="card-body d-flex flex-column">
                        <h5 class="card-title">${video.snippet.title}</h5>
                        <p class="card-text">${video.snippet.description}</p>
                        <a href="${link}" class="btn btn-primary mt-auto">WATCH THIS VIDEO</a>
                      </div>
                    </div>
                `
                })
            }

            result += '</div>'
            $("#results").html(`<div>${result}</div>`);
        },
        error: function (error) {
            console.log(error);
        }
    });
});
