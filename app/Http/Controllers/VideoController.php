<?php


namespace App\Http\Controllers;

use Exception;
use Google_Client;
use Google_Service_YouTube;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class VideoController
 * @package App\Http\Controllers
 */
class VideoController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function search(Request $request): ?JsonResponse
    {
        //Check if user write some keywords in the search field
        if ($request->input('keywords')) {
            try {
                if (!file_exists(base_path() . '/vendor/autoload.php')) {
                    Log::error('Missing google/apiclient package');
                }

                require_once base_path() . '/vendor/autoload.php';

                $client = new Google_Client();
                $client->setApplicationName('Youtube Finder');
                $client->setDeveloperKey(env('YOUTUBE_API_KEY'));
                $client->setScopes([
                    'https://www.googleapis.com/auth/youtube.force-ssl',
                ]);

                $client->setAccessType('offline');

                $service = new Google_Service_YouTube($client);

                /**
                 * q : keywords to match with videos
                 * order : viewCount retrieve the most famous videos
                 * eventType: completed retrieve only uploaded videos, no futures events
                 */
                $queryParams = [
                    'maxResults' => 30,
                    'q' => $request->input('keywords'),
                    'order' => 'viewCount',
                    'eventType' => 'completed',
                    'type' => 'video'
                ];

                $response = $service->search->listSearch('snippet', $queryParams);
                return response()->json(json_encode($response->getItems()), 200);
            } catch (Exception $e) {
                Log::error($e->getMessage());
                return response()->json('error', 500);
            }
        }
        //If there is nothing written then return an empty result
        return response()->json([], 200);
    }
}
