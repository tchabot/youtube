<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Youtube Finder</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500&display=swap" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row justify-content-center mt-md-4">
        <h1 style="font-family: 'Oswald', sans-serif">Youtube Finder</h1>
    </div>
    <div class="row justify-content-center align-items-center mt-md-4">
        <div class="col-2"></div>
        <div class="col-8">
            <input type="text" id="search-input" class="form-control" placeholder="Enter some keywords">
        </div>
        <div class="col-2" style="padding: 0">
            <button type="button" id="search-button" class="btn btn-info">Search</button>
        </div>
    </div>
    <div class="row justify-content-center mt-3" id="results"></div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('js/video.js') }}"></script>
</body>
</html>
