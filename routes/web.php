<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('video-search');
});

Route::get('search', 'VideoController@search');
