# Youtube Finder

Youtube Finder is a Laravel based application which allows you to find videos from keywords.

## Installation

Use the package manager [composer](https://getcomposer.org/) to install Youtube Finder.

```bash
composer install
```

Then run the local server

```bash
php artisan serve
```
